﻿using System;
using System.IO;

namespace ScheduleReader
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
      
            string path = @"C:\Users\Администратор\Downloads\Telegram Desktop\se.xlsx";
            ScheduleReader scheduleReader = (new ScheduleReaderFactory(path)).createInstance();
            if (scheduleReader != null)
            {
                scheduleReader.GetSchedule();
            }
            else
            {
                Console.WriteLine("Not valid");
            }
            return;
        }
    }
}
