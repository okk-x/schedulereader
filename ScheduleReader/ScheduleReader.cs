﻿using OfficeOpenXml;
using ScheduleReader.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleReader
{
    class ScheduleReader
    {
        private ExcelPackage package;
        private ExcelWorksheet worksheet;
        private const short MAX_DAYS= 5;

        public ScheduleReader(FileInfo file, int worksheetId = 0)
        {
            package = new ExcelPackage(file);
            worksheet = package.Workbook.Worksheets[worksheetId];
        }

      
        public List<ScheduleDto> GetSchedule() {
            ExcelRange initCell = GetInitCell();
            if (initCell == null)
            {
                throw new Exception("Invalid file format!");
            }
            List<ScheduleDto> schedules = new();
            foreach(var group in GetGroups(worksheet.Cells[initCell.Start.Row, initCell.Start.Column + 3]))
            {
                
                
                int col = group.Key;
                foreach(var day in GetDayOfWeek(worksheet.Cells[initCell.Start.Row + 2, initCell.Start.Column]))
                {
                    ScheduleDto schedule = new();
                    schedule.DayOfWeek = day.Name;
                    foreach(var lesson in day.lessons)
                    {
                        schedule.Lesson = lesson.Name;
                        foreach(var week in lesson.week)
                        {
                            schedule.Week = week.Key;
                            string mergedCells=worksheet.MergedCells[week.Value[0], col];
                            ExcelRange cell = null;
                            if (!string.IsNullOrEmpty(mergedCells))
                            {
                                ExcelCellAddress address = worksheet.Cells[mergedCells].Start;
                                cell = worksheet.Cells[address.Row,address.Column];
                            }
                            else
                            {
                                cell= worksheet.Cells[week.Value[0], col]; 
                            }
                          

                            if (string.IsNullOrEmpty(cell.Value?.ToString())) continue;

                            int count = 0;
                            while (week.Value[1] >= cell.End.Row)
                            {
                                string cellValue = cell.Value?.ToString().Trim();
                                cell = worksheet.Cells[cell.Start.Row + 1, col];

                                if (cellValue == null) continue;

                                string[] segments = cellValue.Split("ауд.");
                                switch (count)
                                {
                                    case 0:
                                        schedule.Subject = cellValue;
                                        break;
                                    case 1:
                                        schedule.Teacher = segments[0].Trim();
                                        if (segments.Length > 1)
                                        {
                                            schedule.Auditorium = segments[1].Trim();
                                        }
                                        break;
                                    case 3:
                                        if (segments[0].Length > 0)
                                        {
                                            schedule.Teacher = segments[0].Trim();
                                        }
                                        schedule.Auditorium = segments[1].Trim();
                                        break;
                                }
                                count++;
                            }

                            
                            foreach(var studentGroup in group.Value.Split(", "))
                            {
                                ScheduleDto clone = (ScheduleDto)schedule.Clone();
                                clone.StudentGroup=studentGroup.Trim();
                                schedules.Add(clone);
                            }
                        }
                    }
                }
            }

            return schedules;
        }


        protected ExcelRange GetInitCell()
        {
            string cellValue = "";
            for (int row = 1; row <= worksheet.Dimension.Rows; row++)
            {
                for (int col = 1; col <= worksheet.Dimension.Columns; col++)
                {
                    cellValue = worksheet.Cells[row, col].Value?.ToString();
                    if (string.IsNullOrEmpty(cellValue)) continue;
                    if (cellValue.Contains("день неділі"))
                        return worksheet.Cells[row, col];
                }
            }
            return null;
        }

        public abstract class ScheduleObject
        {
            public string Name { get; }
            public int[] Range { get; }

            public ScheduleObject(string name, int[] range)
            {
                Name = name;
                Range = range;
            }
        }
        public class Lesson : ScheduleObject
        {
            public readonly Dictionary<short, int[]> week;

            public Lesson(string name, int[] range, Dictionary<short, int[]> week) : base(name, range) { this.week = week; }

        }
        public class Day: ScheduleObject
        {
            public readonly List<Lesson> lessons = new();
            public Day(string name, int[] range, List<Lesson> lessons) : base(name, range) { this.lessons = lessons; }
        }

        protected Dictionary<int,string> GetGroups(ExcelRange startCell)
        {
            string cellValue = startCell.Value?.ToString();
            int row = startCell.Start.Row,
                col = startCell.Start.Column;

            Dictionary<int,string> groups = new();

            while (!string.IsNullOrEmpty(cellValue))
            {
                groups.Add(col, cellValue);
                cellValue = worksheet.Cells[row, ++col].Value?.ToString();
            }

            return groups;
        }

        protected Dictionary<short, int[]> GetWeek(ExcelRange excelRange)
        {
            ExcelRange cell = worksheet.Cells[worksheet.MergedCells[excelRange.Start.Row, excelRange.Start.Column + 1]];
            Dictionary<short, int[]> week = new();

            int row = excelRange.Start.Row;
            while (excelRange.End.Row >= cell.End.Row)
            {
                week.Add(Convert.ToInt16(worksheet.Cells[cell.Start.Row, cell.Start.Column].Value?.ToString()), new int[] { cell.Start.Row, cell.End.Row });
                string mergedCells = worksheet.MergedCells[cell.End.Row + 1, cell.Start.Column];
                if (string.IsNullOrEmpty(mergedCells)) break;
                cell = worksheet.Cells[mergedCells];
            }

            return week;
        }

        protected List<Lesson> GetLessons(ExcelRange excelRange)
        {
            ExcelRange cell = worksheet.Cells[worksheet.MergedCells[excelRange.Start.Row, excelRange.Start.Column+1]];
            List<Lesson> lessons = new();

            int row = excelRange.Start.Row;
            while (excelRange.End.Row >= cell.End.Row)
            {
                lessons.Add(new Lesson(worksheet.Cells[cell.Start.Row, cell.Start.Column].Value?.ToString(), new int[] { cell.Start.Row, cell.End.Row }, GetWeek(cell)));
                string mergedCells = worksheet.MergedCells[cell.End.Row + 1, cell.Start.Column];
                if (string.IsNullOrEmpty(mergedCells)) break;
                cell = worksheet.Cells[mergedCells];
            }

            return lessons;
        }

        protected IEnumerable<Day> GetDayOfWeek(ExcelRange startCell)
        {
            short countDays = MAX_DAYS;
            List<Day> days = new();
            int row = startCell.Start.Row, column = startCell.Start.Column;
            while (countDays > 0)
            {
                ExcelRange cell = worksheet.Cells[worksheet.MergedCells[row, column]];

                yield return new Day(worksheet.Cells[cell.Start.Row, cell.Start.Column].Value?.ToString(),
                    new int[] { cell.Start.Row, cell.End.Row },
                    GetLessons(cell));

                row = cell.End.Row+1;
                countDays--;
            }

        }

    }
}
