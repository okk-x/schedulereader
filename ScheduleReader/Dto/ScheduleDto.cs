﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleReader.Dto
{
    class ScheduleDto: ICloneable
    {
        public string StudentGroup { get; set; }
        public string Subject { get; set; }
        public string Teacher { get; set; }
        public string DayOfWeek { get; set; }
        public short Week { get; set; } 
        public string Lesson { get; set; } 
        public string Auditorium { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
