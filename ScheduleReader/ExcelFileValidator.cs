﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleReader
{
    class ExcelFileValidator
    {
        private readonly FileInfo file;
        private readonly List<string> AcceptableFileExtensions = new() { "xlsx" };
        public ExcelFileValidator(FileInfo file) => this.file = file;

        public bool Validate() => file.Exists && AcceptableFileExtensions.Contains(file.Extension.Trim('.'));
    }
}
