﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleReader
{
    class ScheduleReaderFactory
    {
        private FileInfo file;

        public ScheduleReaderFactory(string pathToScheduleFile)
        {
            this.file = new FileInfo(pathToScheduleFile);
        }

        public ScheduleReader createInstance()
        {
            if((new ExcelFileValidator(this.file)).Validate())
            {
                return new ScheduleReader(this.file);
            }
            return null;
        }

    }
}
