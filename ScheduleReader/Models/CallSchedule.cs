﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleReader.Models
{
    class CallSchedule
    {
        public int Id { get; set; }
        public short Lesson { get; set; }
        public DateTime StartOfLecture { get; set; }
        public DateTime EndOfLecture { get; set; }

    }
}
