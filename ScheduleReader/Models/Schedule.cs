﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleReader.Models
{
    class Schedule
    {
        public int Id { get; set;}
        public int GroupId { get; set; }
        public int SubjectId { get; set; }
        public int TeacherId { get; set; }
        public short DayOfWeek { get; set; }
        public bool Week { get; set; }
        public short Lesson { get; set; }
        public string Auditorium { get; set; }
     }
}
